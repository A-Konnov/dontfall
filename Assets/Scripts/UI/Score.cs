﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score : MonoBehaviour
{
    [SerializeField] private CubeShift m_controller;
    [SerializeField] private TextMeshProUGUI m_text;
    private int m_score;
    public int GetScore
    {
        get { return m_score; }
    }

    private void Start()
    {
        m_score = 0;
        gameObject.SetActive(false);
        EventManager.Instance.CubeInPlace.AddListener(ScoreAdd);
        EventManager.Instance.GameEnd.AddListener(SaveScore);
    }

    private void OnDestroy()
    {
        EventManager.Instance.CubeInPlace.RemoveListener(ScoreAdd);
        EventManager.Instance.GameEnd.RemoveListener(SaveScore);
    }

    private void ScoreAdd()
    {
        m_score++;
        m_text.text = string.Format("{0}", m_score);
    }

    private void SaveScore()
    {
        var score = PlayerPrefs.GetInt("Score");
        if (m_score > score)
        {
            PlayerPrefs.SetInt("Score", m_score);
        }
    }
}
