﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StartGame : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_coins;
    [SerializeField] private TextMeshProUGUI m_numberOfGames;
    [SerializeField] private TextMeshProUGUI m_bestScore;
    [SerializeField] private GameObject m_score;


    private void Awake()
    {
        Time.timeScale = 0;
    }

    private void Start()
    {
        m_coins.text = string.Format("coins: {0}", PlayerPrefs.GetInt("Coins"));
        m_numberOfGames.text = string.Format("number of games: {0}", PlayerPrefs.GetInt("NumberOfGames"));
        m_bestScore.text = string.Format("best score: {0}", PlayerPrefs.GetInt("Score"));
    }

    public void OnClickStart()
    {
        Time.timeScale = 1;
        var animator = GetComponent<Animator>();
        animator.SetTrigger("Move");
        m_score.SetActive(true);
    }

}
