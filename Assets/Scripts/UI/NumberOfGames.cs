﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberOfGames : MonoBehaviour
{
    private void Start()
    {
        EventManager.Instance.GameEnd.AddListener(AddNumberOfGames);
    }

    private void OnDestroy()
    {
        EventManager.Instance.GameEnd.RemoveListener(AddNumberOfGames);
    }

    private void AddNumberOfGames()
    {
        var numberOfGames = PlayerPrefs.GetInt("NumberOfGames");
        PlayerPrefs.SetInt("NumberOfGames", ++numberOfGames);
    }
}
