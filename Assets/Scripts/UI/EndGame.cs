﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class EndGame : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_coins;
    [SerializeField] private TextMeshProUGUI m_numberOfGames;
    [SerializeField] private TextMeshProUGUI m_bestScore;

    private void Start()
    {
        EventManager.Instance.GameEnd.AddListener(Activate);
    }

    private void OnDestroy()
    {
        EventManager.Instance.GameEnd.RemoveListener(Activate);
    }

    private void Activate()
    {
        var animator = GetComponent<Animator>();
        animator.SetTrigger("Move");

        m_coins.text = string.Format("coins: {0}", PlayerPrefs.GetInt("Coins"));
        m_numberOfGames.text = string.Format("number of games: {0}", PlayerPrefs.GetInt("NumberOfGames"));
        m_bestScore.text = string.Format("best score: {0}", PlayerPrefs.GetInt("Score"));
    }

    public void OnClick()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}
