﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SphereDead : MonoBehaviour
{
    private bool m_gameEnd = false;

    private void Update()
    {
        if (transform.position.y < 0.5f && !m_gameEnd)
        {
            m_gameEnd = true;
            gameObject.GetComponent<Collider>().isTrigger = true;
            var camera = GetComponentInChildren<Camera>();
            if (camera != null)
            {
                camera.transform.parent = null;
                EventManager.Instance.GameEnd.Invoke();
            }
            
        }
    }
}
