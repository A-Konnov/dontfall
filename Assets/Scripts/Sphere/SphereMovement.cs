﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SphereMovement : MonoBehaviour
{
    [Header("Reference:")]
    [SerializeField] private GeneratorLines m_generatorlines;

    [Header("Property:")]
    [SerializeField] private float m_speed;

    private Vector3 m_point;
    public Vector3 Point
    {
        get
        {
            return m_point;
        }
    }

    public UnityEvent CheckPoint = new UnityEvent();


    private void Start()
    {
        GeneratePoint();
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, m_point, m_speed * Time.deltaTime);
        if (transform.position == m_point)
        {
            CheckPoint.Invoke();
            GeneratePoint();
        }

    }

    private void GeneratePoint()
    {
        m_point = m_generatorlines.Point;
        m_point.y = transform.position.y;
    }
}
