﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShift : MonoBehaviour
{
    [SerializeField] private SphereMovement m_sphere;
    [SerializeField] private float m_distanceToShift = 6.5f;
    [SerializeField] private float m_shiftSpeed = 6.7f;

    private Vector3 m_newPosition;

    private void Update()
    {
        var distance = (m_sphere.Point - transform.root.position).sqrMagnitude;
        if (distance <= m_distanceToShift * m_distanceToShift)
        {
            if (m_newPosition == Vector3.zero)
            {
                m_newPosition = transform.localPosition;
                var tmpX = m_newPosition.x;
                m_newPosition.x = m_newPosition.z;
                m_newPosition.z = tmpX;
            }

            transform.localPosition = Vector3.MoveTowards(transform.localPosition, m_newPosition, m_shiftSpeed * Time.deltaTime);
            if (transform.localPosition == m_newPosition)
            {
                var pos = transform.localPosition;
                pos.x = Mathf.Floor(pos.x);
                pos.y = Mathf.Floor(pos.y);
                transform.localPosition = pos;

                m_newPosition = Vector3.zero;
            }
        }
    }
}
