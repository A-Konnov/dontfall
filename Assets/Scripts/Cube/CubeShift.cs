﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeShift : MonoBehaviour
{
    [SerializeField] private SOPullObject m_pullCube;

    private Vector2 m_mousePositionButtonDown;
    private Vector2 m_mousePositionButtonUp;
    private List<GameObject[]> m_allLines;

    private void Start()
    {
        m_allLines = GetComponent<GeneratorLines>().AllLines;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            m_mousePositionButtonDown = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            m_mousePositionButtonUp = Input.mousePosition;
            if (Mathf.Abs(m_mousePositionButtonDown.x - m_mousePositionButtonUp.x) > 20f)
            {
                if (m_mousePositionButtonDown.x < m_mousePositionButtonUp.x)
                {
                    //right
                    MoveCube(1);
                }
                else
                {
                    //left
                    MoveCube(-1);
                }
            }
        }
    }


    private void MoveCube(int shift)
    {
        for (int i = 0; i < m_allLines.Capacity; i++)
        {
            GameObject[] line = m_allLines[i];
            for (int j = 0; j < line.Length; j++)
            {
                if (line[j].GetComponent<Collider>().isTrigger == true)
                {
                    var pos = line[j].transform.position;
                    var correctPos = line[j - 1].transform.position;

                    if (line[j].transform.rotation.eulerAngles == Vector3.zero)
                    {
                        pos.x = pos.x - shift;
                        correctPos.z = correctPos.z + 2f;
                    }
                    else
                    {
                        pos.z = pos.z + shift;
                        correctPos.x = correctPos.x + 2f;
                    }

                    line[j].transform.position = pos;
                    if (pos == correctPos)
                    {
                        var newCube = m_pullCube.GetPullObject();
                        newCube.transform.position = line[j].transform.position;
                        newCube.transform.rotation = line[j].transform.rotation;
                        newCube.SetActive(true);
                        line[j].SetActive(false);
                        line[j] = newCube;
                        EventManager.Instance.CubeInPlace.Invoke();
                    }

                    return;
                }
            }
        }
    }
}
