﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinActivation : MonoBehaviour
{
    [SerializeField] private GameObject m_trail;
    private Animator m_animator;

    private void Start()
    {
        m_animator = GetComponent<Animator>();
        m_trail.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<SphereMovement>() != null)
        {
            m_animator.SetTrigger("Activate");
            EventManager.Instance.CoinCollected.Invoke();
        }
    }

    //called in animation
    public void Deactivate()
    {
        m_trail.SetActive(false);
        gameObject.SetActive(false);
    }
}
