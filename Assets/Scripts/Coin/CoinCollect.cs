﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCollect : MonoBehaviour
{
    private int m_coins;

    private void Start()
    {
        EventManager.Instance.CoinCollected.AddListener(AddCoin);
        EventManager.Instance.GameEnd.AddListener(SaveCoin);
    }

    private void OnDestroy()
    {
        EventManager.Instance.CoinCollected.RemoveListener(AddCoin);
        EventManager.Instance.GameEnd.RemoveListener(SaveCoin);
    }

    private void AddCoin()
    {
        m_coins++;
    }

    private void SaveCoin()
    {
        var coins = PlayerPrefs.GetInt("Coins");
        PlayerPrefs.SetInt("Coins", coins + m_coins);
    }

}
