﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SOPullObject : ScriptableObject
{
    [SerializeField] private GameObject m_prefab;
    [SerializeField] private int m_amount;

    private List<GameObject> m_cubes;
    public List<GameObject> Cubes
    {
        get { return m_cubes; }
    }

    public void GeneratePull()
    {
        m_cubes = new List<GameObject>();

        for (int i = 0; i < m_amount; i++)
        {
            var cube = Instantiate(m_prefab);
            cube.SetActive(false);
            m_cubes.Add(cube);
        }
    }

    public GameObject GetPullObject()
    {
        for (int i = 0; i < m_amount; i++)
        {
            if (m_cubes[i].activeSelf == false)
            {
                return m_cubes[i];
            }
        }

        var cube = Instantiate(m_prefab);
        m_cubes.Add(cube);
        return cube;
    }
}
