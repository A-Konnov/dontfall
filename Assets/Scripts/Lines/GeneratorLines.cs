﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GeneratorLines : MonoBehaviour
{
    [Header("Reference:")]
    [SerializeField] private SOPullObject m_pullCube;
    [SerializeField] private SOPullObject m_pullCubeMove;
    [SerializeField] private SOPullObject m_pullCoins;
    [SerializeField] private Transform m_startGenerationLine;

    [Header("Property:")]
    [SerializeField] private int m_minLenghtline = 5;
    [SerializeField] private int m_maxLenghtline = 20;
    [SerializeField] private int m_amountCubeMoveOnFirstLine = 2;
    [SerializeField] private bool m_cubeMove;

    private Vector3 m_lastCube;
    private float m_amountCubeMove;
    private SphereMovement m_sphere;

    private List<GameObject[]> m_allLines;
    public List<GameObject[]> AllLines
    {
        get
        {
            return m_allLines;
        }
    }

    private List<Vector3> m_points;
    public Vector3 Point
    {
        get
        {
            var point = m_points[0];
            m_points.RemoveAt(0);
            return point;
        }
    }

    private enum Direction { left, right };
    private Direction m_lineDirection;


    private void Awake()
    {
        m_allLines = new List<GameObject[]>();
        m_points = new List<Vector3>();

        m_pullCube.GeneratePull();
        m_pullCubeMove.GeneratePull();
        m_pullCoins.GeneratePull();

        m_amountCubeMove = m_amountCubeMoveOnFirstLine;
        m_lastCube = m_startGenerationLine.position;

        m_lineDirection = Direction.left;

        m_allLines.Add(GenerateLine(m_lastCube, m_lineDirection));
        m_allLines.Add(GenerateLine(m_lastCube, m_lineDirection));
    }

    private void Start()
    {
        m_sphere = FindObjectOfType<SphereMovement>();
        m_sphere.CheckPoint.AddListener(GenerateLine);
    }

    private void OnDestroy()
    {
        m_sphere.CheckPoint.RemoveListener(GenerateLine);
    }

    private void GenerateLine()
    {
        if (m_allLines.Count > 3)
        {
            var line = m_allLines[0];
            foreach (var cube in line)
            {
                cube.SetActive(false);
            }
            m_allLines.RemoveAt(0);
        }
        m_allLines.Add(GenerateLine(m_lastCube, m_lineDirection));
    }

    private GameObject[] GenerateLine(Vector3 position, Direction direction)
    {
        int lengthLine = Random.Range(m_minLenghtline, m_maxLenghtline);
        int[] difficult = GenerateDifficult((int)Mathf.Clamp((Mathf.Round(m_amountCubeMove)), 0, lengthLine - 1), lengthLine);
        m_amountCubeMove = m_amountCubeMove + 0.3f;

        GameObject[] line = new GameObject[difficult.Length];

        if (m_lineDirection == Direction.left)
        {
            position.z = position.z + 0.5f;
            position.x = position.x - 1.5f;
        }
        else
        {
            position.x = position.x + 0.5f;
            position.z = position.z - 1.5f;
        }

        for (int i = 0; i < difficult.Length; i++)
        {
            if (difficult[i] == 1)
            {
                position = PlaceCubeMove(position, line, i);
            }
            else
            {
                position = PlaceCube(position, line, i);
                PlaceCoin(position);
            }
        }

        m_lastCube = line[line.Length - 1].transform.position;
        m_points.Add(DefinePoint(line[0]));
        m_lineDirection = m_lineDirection == Direction.left ? Direction.right : Direction.left;

        return line;
    }

    private Vector3 DefinePoint(GameObject cube)
    {
        var point = cube.transform.position;

        if (m_lineDirection == Direction.left)
        {
            point.x = point.x - 0.5f;
        }
        else
        {
            point.z = point.z - 0.5f;
        }

        return point;
    }

    private void PlaceCoin(Vector3 position)
    {

        int place = Random.Range(0, 5);
        if (place == 0)
        {
            var coin = m_pullCoins.GetPullObject();
            position.y = 0;
            coin.transform.position = position;
            coin.gameObject.SetActive(true);
        }
    }

    private Vector3 PlaceCube(Vector3 position, GameObject[] line, int i)
    {
        line[i] = m_pullCube.GetPullObject();
        line[i].SetActive(true);

        if (m_lineDirection == Direction.left)
        {
            line[i].transform.rotation = Quaternion.Euler(0, 90, 0);
            position.x = position.x + 2;
        }
        else
        {
            line[i].transform.rotation = Quaternion.Euler(0, 0, 0);
            position.z = position.z + 2;
        }

        line[i].transform.position = position;
        return position;
    }

    private Vector3 PlaceCubeMove(Vector3 position, GameObject[] line, int i)
    {
        line[i] = m_pullCubeMove.GetPullObject();
        line[i].SetActive(true);

        int shiftDirection = Random.Range(0, 2);

        Vector3 posCubeMove;

        if (m_lineDirection == Direction.left)
        {
            position.x = position.x + 2;
            posCubeMove = shiftDirection == 0 ? position - Vector3.forward : position + Vector3.forward;
            line[i].transform.rotation = Quaternion.Euler(0, 90, 0);
        }
        else
        {
            position.z = position.z + 2;
            posCubeMove = shiftDirection == 0 ? position - Vector3.right : position + Vector3.right;
            line[i].transform.rotation = Quaternion.Euler(0, 0, 0);
        }

        line[i].transform.position = posCubeMove;
        return position;
    }

    private int[] GenerateDifficult(int amountCubeMove, int lengthLine)
    {
        int[] difficult = new int[lengthLine];

        for (int i = 0; i < difficult.Length; i++)
        {
            difficult[i] = 0;
        }

        if (amountCubeMove == 0)
        {
            return difficult;
        }


        if (m_cubeMove)
        {
            for (int i = 1; i < amountCubeMove + 1; i++)
            {
                difficult[i] = 1;
            }
        }

        var randomDifficult = difficult.Clone() as int[];
        for (int i = 1; i < randomDifficult.Length - 1; i++)
        {
            int temp = randomDifficult[i];
            int r = Random.Range(i, randomDifficult.Length - 1);
            randomDifficult[i] = randomDifficult[r];
            randomDifficult[r] = temp;
        }

        return randomDifficult;
    }
}
