﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplaceColor : MonoBehaviour
{
    [Header("Reference:")]
    [SerializeField] private Material m_material;

    [Header("Color settings:")]
    [SerializeField] private Color[] m_colors;

    [Header("Time settings:")]
    [SerializeField] private float m_delay = 5f;
    [SerializeField] private float m_speedChangeColor = 0.1f;

    private Color m_nextColor;
    private int m_index = 0;
    private bool m_playCoroutine = false;

    private void OnDestroy()
    {
        m_material.color = m_colors[0];
    }

    private void Update()
    {
        m_material.color = Color.Lerp(m_material.color, m_colors[m_index], m_speedChangeColor);
        if (m_material.color == m_colors[m_index] && !m_playCoroutine)
        {
            StartCoroutine(ChangeColor());
        }        
    }

    private IEnumerator ChangeColor()
    {
        m_playCoroutine = true;
        yield return new WaitForSeconds(m_delay);
        m_nextColor = m_colors[m_index];
        if (m_index < m_colors.Length - 1)
        {
            m_index++;
        }
        else
        {
            m_index = 0;
        }
        m_playCoroutine = false;
    }



}
